package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DBManager {

    private static final String SQL_CREATE_USER = "insert into users values (default, ?)";
    private static final String SQL_DELETE_TEAM = "delete from teams where name=?";
    private static final String SQL_DELETE_USERS = "delete from users where id=?";
    private static final String SQL_CREATE_TEAM = "insert into teams values (default, ?)";
    private static final String SQL_SET_TEAM_FOR_USER = "insert into users_teams values (?, ?)";
    private static final String SQL_FIND_USER_BY_LOGIN = "select * from users where login=?";
    private static final String SQL_FIND_TEAM_BY_NAME = "select * from teams where name =?";
    private static final String SQL_UPDATE_TEAM = "update teams set name=? where id=?";

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }


    public Connection connect() throws SQLException, IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("app.properties"));

        String url = properties.getProperty("connection.url");
        return DriverManager.getConnection(url);
    }

    public List<User> findAllUsers() throws DBException {
        List<User> userList = new ArrayList<>();
        Connection con = null;
        ResultSet rs = null;
        try {
            con = connect();
            Statement stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM users");
            while (rs.next()) {
                User user = new User();
                user.setLogin(rs.getString(2));
                user.setId(rs.getInt(1));
                userList.add(user);
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            close(con);
        }
        return userList;
    }

    public boolean insertUser(User user) {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = connect();
            pstmt = con.prepareStatement(
                    SQL_CREATE_USER,
                    Statement.RETURN_GENERATED_KEYS);

            int k = 1;
            pstmt.setString(k++, user.getLogin());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                }
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(con);
        }
        return true;
    }


    public boolean deleteUsers(User... users) throws DBException {

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = connect();
            pstmt = con.prepareStatement(SQL_DELETE_USERS);
            for (User user : users) {
                pstmt.setInt(1, user.getId());
                pstmt.executeUpdate();
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            close(con);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = null;

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = connect();
            pstmt = con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
            pstmt.setString(1, login);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                user = User.createUser(rs.getString(2));
                user.setId(rs.getInt(1));
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            close(con);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = connect();
            pstmt = con.prepareStatement(SQL_FIND_TEAM_BY_NAME);
            pstmt.setString(1, name);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                team = Team.createTeam(rs.getString(2));
                team.setId(rs.getInt(1));
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            close(con);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teamList = new ArrayList<>();
        Connection con = null;
        ResultSet rs = null;
        try {
            con = connect();
            Statement stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM teams");
            while (rs.next()) {
                Team team = new Team();
                team.setName(rs.getString(2));
                team.setId(rs.getInt(1));
                teamList.add(team);
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            close(con);
        }
        return teamList;
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = connect();
            pstmt = con.prepareStatement(
                    SQL_CREATE_TEAM,
                    Statement.RETURN_GENERATED_KEYS);

            int k = 1;
            pstmt.setString(k++, team.getName());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                }
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(con);
        }
        return true;
    }


    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = connect();
            con.setAutoCommit(false);
            pstmt = con.prepareStatement(
                    SQL_SET_TEAM_FOR_USER,
                    Statement.RETURN_GENERATED_KEYS);

            pstmt.setInt(1, user.getId());
            for (Team team : teams) {
                pstmt.setInt(2, team.getId());
                pstmt.execute();
            }
            con.commit();
        } catch (SQLException | IOException e) {
            // (1) write to logger!
            e.printStackTrace();

            // (2) rollback transaction
            rollback(con);

            // (3) throw new DBException("Cannot insert users: " + users, ex);
            throw new DBException("Cannot insert users: " + user.getLogin(), e);
        } finally {
            close(con);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teamList = new ArrayList<>();
        List<Integer> teamsId = new ArrayList<>();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = connect();
            pstmt = con.prepareStatement("select * from users_teams where user_id=?");
            pstmt.setInt(1, user.getId());

            rs = pstmt.executeQuery();
            while (rs.next()) {
                teamsId.add(rs.getInt(2));
            }

            pstmt = con.prepareStatement("select * from teams where id=?");
            for (Integer id : teamsId) {
                pstmt.setInt(1, id);
                rs = pstmt.executeQuery();

                if (rs.next()) {
                    Team team = new Team();
                    team = Team.createTeam(rs.getString(2));
                    team.setId(rs.getInt(1));
                    teamList.add(team);
                }
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            close(con);
        }
        return teamList;
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = connect();
            pstmt = con.prepareStatement(SQL_DELETE_TEAM);
            pstmt.setString(1, team.getName());
            pstmt.executeUpdate();

        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            close(con);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = connect();
            pstmt = con.prepareStatement(SQL_UPDATE_TEAM);

            int k = 1;
            pstmt.setString(k++, team.getName());
            pstmt.setInt(k++, team.getId());

            pstmt.executeUpdate();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        } finally {
            close(con);
        }
        return true;
    }

    private void close(Connection cn) {
        try {
            if (cn != null) {
                cn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
